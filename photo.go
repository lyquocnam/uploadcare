package uploadcare

import "github.com/jinzhu/gorm"

const PHOTO_TABLE = "photos"

type Photo struct {
	ID  string `gorm:"primary_key" json:"id"`
	URL string `json:"url"`
}

func (u *Photo) AfterDelete(tx *gorm.DB) (err error) {
	Uploader().RemoveFromCloud(u.ID)
	return nil
}
