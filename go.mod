module gitlab.com/lyquocnam/uploadcare

require (
	github.com/gin-gonic/gin v1.3.0
	github.com/jinzhu/gorm v1.9.2
	gitlab.com/lyquocnam/core v0.0.0-20181212143634-75f42e336703
	gopkg.in/resty.v1 v1.10.3
)
