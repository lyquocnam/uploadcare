package uploadcare

import (
	"encoding/json"
	"fmt"

	"io/ioutil"
	"mime/multipart"
	"os"
	"sync"

	"github.com/gin-gonic/gin"
	"gitlab.com/lyquocnam/core"

	resty "gopkg.in/resty.v1"
)

type uploader struct {
	fileId string
}

var instance *uploader
var one sync.Once

func Uploader() *uploader {
	one.Do(func() {
		instance = &uploader{}
	})
	return instance
}

func (u *uploader) Upload(c *gin.Context, file *multipart.FileHeader) *Photo {
	// Upload image lên temp file
	core.CreateTmpFolderIfNotExist()

	f, err := ioutil.TempFile("tmp", "*")
	if err != nil {
		fmt.Errorf("Không thể tạo tmp file để upload")
		return nil
	}
	tmpFileName := f.Name()
	defer os.Remove(tmpFileName)

	if err := c.SaveUploadedFile(file, tmpFileName); err != nil {
		fmt.Println(err)
		return nil
	}

	fileId, err := u.UploadToCloud(tmpFileName)
	if err != nil {
		return nil
	}

	fileURL, err := u.GetFromCloud(fileId)
	if err != nil {
		return nil
	}

	photo := &Photo{
		ID:  fileId,
		URL: fileURL,
	}

	return photo
}

func (u *uploader) GetFromCloud(fileId string) (string, error) {
	res, err := resty.R().
		SetHeader("Authorization",
			fmt.Sprintf(`Uploadcare.Simple %v:%v`, core.Config.UploadCarePublicKey, core.Config.UploadCarePrivateKey)).
		Get(fmt.Sprintf(`https://api.uploadcare.com/files/%v/`, fileId))

	if err != nil {
		return "", nil
	}

	if res.StatusCode() == 200 {
		data := map[string]string{}
		json.Unmarshal(res.Body(), &data)
		return data["original_file_url"], nil
	}

	return "", fmt.Errorf("Không thể upload file")
}

func (u *uploader) UploadToCloud(filaPath string) (string, error) {
	res, err := resty.R().
		SetFile("file", filaPath).
		SetFormData(map[string]string{
			"UPLOADCARE_PUB_KEY": core.Config.UploadCarePublicKey,
			"UPLOADCARE_STORE":   "1",
		}).
		Post("https://upload.uploadcare.com/base/")

	if err != nil {
		fmt.Println(err)
		return "", nil
	} else if res.StatusCode() == 200 {
		data := map[string]string{}
		json.Unmarshal(res.Body(), &data)
		fileId := data["file"]
		return fileId, nil
	}
	return "", nil
}

func (u *uploader) RemoveFromCloud(fileId string) bool {

	res, err := resty.R().
		SetHeader("Authorization", fmt.Sprintf(`Uploadcare.Simple %v:%v`, core.Config.UploadCarePublicKey, core.Config.UploadCarePrivateKey)).
		Delete(fmt.Sprintf(`https://api.uploadcare.com/files/%v/storage/`, fileId))
	if err != nil {
		fmt.Println(err)
		return false
	}
	return res.StatusCode() == 200
}
